
----------------------------------------------------------------- update settings ----------------------------------------------------------------- 

IF (SELECT Count(*) FROM Setting where [Name] = 'smartproductcollectionssettings.showitemsincarousel-defaultclean') = 0
BEGIN
	Insert Into Setting Values ('smartproductcollectionssettings.showitemsincarousel-defaultclean', 'True', 0)
	Insert Into Setting Values ('smartproductcollectionssettings.showitemsincarousel-unirepair', 'True', 0)
END


IF (SELECT Count(*) FROM Setting where [Name] = 'smartproductcollectionssettings.slickproductsettings-defaultclean') = 0
BEGIN
	Insert Into Setting Values ('smartproductcollectionssettings.slickproductsettings-defaultclean', '{ "infinite": true, "rows":1, "slidesToShow": 4, "arrows": true, "dots": false, "easing": "swing", "draggable": false,"autoplay": true, "autoplaySpeed": 2000 }	', 0)
END

Go
----------------------------------------------------------------- ----------------------------------------------------------------- ----------------------------------------------------------------- 


----------------------------------------------------------------- insert resources values ----------------------------------------------------------------- 
--IF (SELECT Count(*) FROM LocaleStringResource where ResourceName = 'Footer.MyAccount') = 0
--BEGIN
--	Insert Into LocaleStringResource Values (1, 'Footer.MyAccount', 'Dashboard')
--END


Update LocaleStringResource Set ResourceValue = 'About Us' Where ResourceName = 'Footer.FollowUs'
Update LocaleStringResource Set ResourceValue = 'Our Info' Where ResourceName = 'Footer.MyAccount'

--INSERT INTO localestringresource (LanguageId, ResourceName, ResourceValue)
--SELECT 2, ResourceName, ResourceValue
--FROM localestringresource
--WHERE languageid = 1 
--and (resourcename like '%SevenSpikes%' OR resourcename like '%clientreviews%' OR resourcename like '%custommegamenu%')

----------------------------------------------------------------- ----------------------------------------------------------------- ----------------------------------------------------------------- 

