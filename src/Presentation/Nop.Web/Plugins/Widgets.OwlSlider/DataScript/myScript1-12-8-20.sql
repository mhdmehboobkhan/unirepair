



  ----------------------------------------------------------------- resources ----------------------------------------------------------------- 

IF (SELECT Count(*) FROM LocaleStringResource where ResourceName = 'Plugins.Widgets.OwlSlider.MobilePicture') = 0
BEGIN
	INSERT INTO LocaleStringResource VALUES(1, 'Plugins.Widgets.OwlSlider.MobilePicture', 'Mobile Picture')
	INSERT INTO LocaleStringResource VALUES(1, 'Plugins.Widgets.OwlSlider.MobilePicture.Hint', 'Upload mobile picture.')
END

GO
----------------------------------------------------------------- ----------------------------------------------------------------- ----------------------------------------------------------------- 
